package com.listecourses.services;

import com.listecourses.beans.Category;
import com.listecourses.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository repository;

    public List<Category> getCategories() {

        List<Category> categories = new ArrayList<>();

        repository.findAll().forEach(categories::add);

         return categories;
    }

}
